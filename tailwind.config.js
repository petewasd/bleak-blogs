const colors = require('tailwindcss/colors')
module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                transparent: "transparent",
                current: "currentColor",
                orange: colors.orange,
                teal: colors.teal,
                amber: colors.amber,
                lime: colors.lime,
                emerald: colors.emerald,
                fuchsia: colors.fuchsia,
                rose: colors.rose,
                violet: colors.violet,
                coolGray: colors.coolGray,
                prussianBlue: {
                    DEFAULT: '#023047',
                    '50': '#35C7F9',
                    '100': '#1CBEF9',
                    '200': '#069FDC',
                    '300': '#0579AA',
                    '400': '#035379',
                    '500': '#023047',
                    '600': '#022538',
                    '700': '#011B29',
                    '800': '#01111A',
                    '900': '#00070B'
                },
                cornFlowerBlue: {
                    DEFAULT: '#8ECAE6',
                    '50': '#FFFFFF',
                    '100': '#FFFFFF',
                    '200': '#F2F9FC',
                    '300': '#D1E9F5',
                    '400': '#AFDAED',
                    '500': '#8ECAE6',
                    '600': '#60B4DC',
                    '700': '#329FD2',
                    '800': '#257DA7',
                    '900': '#1B5B79'
                },
                blueGreen: {
                    DEFAULT: '#219EBC',
                    '50': '#A5E1EF',
                    '100': '#94DBEC',
                    '200': '#71D0E6',
                    '300': '#4FC4E0',
                    '400': '#2CB8DA',
                    '500': '#219EBC',
                    '600': '#19768C',
                    '700': '#104E5D',
                    '800': '#08262D',
                    '900': '#000000'
                },
                honeyYellow: {
                    DEFAULT: '#FFB703',
                    '50': '#FFEBBB',
                    '100': '#FFE6A6',
                    '200': '#FFDA7D',
                    '300': '#FFCE55',
                    '400': '#FFC32C',
                    '500': '#FFB703',
                    '600': '#CA9000',
                    '700': '#926800',
                    '800': '#5A4000',
                    '900': '#221800'
                },
                // orangePeel: {  DEFAULT: '#FF9E00',  50: '#FFF5E5',  100: '#FFECCC',  200: '#FFD899',  300: '#FFC566',  400: '#FFB133',  500: '#FF9E00',  600: '#CC7E00',  700: '#995F00',  800: '#663F00',  900: '#332000'},
                // flushOrange: {  DEFAULT: '#FF8500',  50: '#FFF3E5',  100: '#FFE7CC',  200: '#FFCE99',  300: '#FFB666',  400: '#FF9D33',  500: '#FF8500',  600: '#CC6A00',  700: '#995000',  800: '#663500',  900: '#331B00'},
                // persianIndigo: {  DEFAULT: '#3C096C',  50: '#AF68F2',  100: '#A351F0',  200: '#8A21ED',  300: '#7011CA',  400: '#560D9B',  500: '#3C096C',  600: '#22053D',  700: '#08010E',  800: '#000000',  900: '#000000'},
                // russiaViolet: {  DEFAULT: '#240046',  50: '#992DFF',  100: '#8C13FF',  200: '#7300DF',  300: '#5800AC',  400: '#3E0079',  500: '#240046',  600: '#0A0013',  700: '#000000',  800: '#000000',  900: '#000000'},
            },
        },
        fontFamily: {
            lato: "'Lato', sans-serif",
            poppins: "'Poppins', sans-serif",
            exo: "'Exo', sans-serif",
            bungeeShade: "'Bungee Shade', cursive",
            monoton: "'Monoton', cursive",
            quicksand: "'Quicksand', sans-serif",
            spacemono: "'Space Mono', monospace",
            nunito: "'Nunito', sans-serif",
            blinker: "'Blinker', sans-serif"
        }
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
