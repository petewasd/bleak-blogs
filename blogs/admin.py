from django.contrib import admin
from compressor.filters import CompilerFilter
from .models import Post

admin.site.register(Post)


# Register your models here.

class PostCSSFilter(CompilerFilter):
    command = 'postcss'
