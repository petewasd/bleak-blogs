import requests

from django.shortcuts import render
from .models import Post

dummy_posts = [
    {
        'author': 'Pete',
        'title': 'Writing ABC',
        'content': 'ABC basics',
        'date_posted': 'December 2, 2021'
    },
    {
        'author': 'Spock',
        'title': 'Cobol Basics',
        'content': 'Way boring stuff',
        'date_posted': 'May 4, 2351'
    }
]


def get_random_prog_quote():
    resp = requests.get(url="https://programming-quotes-api.herokuapp.com/Quotes/random")
    random_quote = resp.json()
    quote = random_quote['en']
    author = random_quote['author']
    return {
        'quote': quote,
        'author': author
    }


# Create your views here.
def index(request):
    prog_quote_context = get_random_prog_quote()
    return render(request, 'index.html', context=prog_quote_context)


def home(request):
    posts = Post.objects.all()
    context = {
        'posts': posts
    }
    return render(request, 'blogs/home.html', context=context)


def about(request):
    return render(request, 'blogs/about.html')
