# bleak-blog

This blog project is created using Django, Tailwind CSS.

## Steps to start project

1. Activate virtual environment `django_venv`

    > macOS `$ source django_venv/bin/activate`
    > 
    > Windows `> .\django-env\Scripts\activate`
   
2. Install node dependencies - TailwindCSS, PostCSS, Autoprefixer
   > `npm install`
   

3. Run in project root `> python manage.py runserver`



#### Optional: Pycharm CE/PRO Interpreter setup

 `Pycharm > Preferences\Settings > Project: bleak-blogs > Python Interpreter > ⚙️(gear box icon) > Add > Existing Environment >`

 Select `django_env\bin\python` from project root

=======
![alt text](/screenshots/bleakblog-quote-index.png)

## TODOs

- [ ] CSS Compressor not working, Admin static files missing

- [ ] Ability to update User profile name, image, email from F/E

- [ ] Password reset on email

- [ ] CRUD routes for posts

- [ ] Check if Tailwind deps need to be downloaded in local config
